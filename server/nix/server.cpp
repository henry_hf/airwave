/*
 * Original source found at http://codebase.eu/tutorial/linux-socket-programming-c/code/tcpclient.cpp
 */
#include <iostream>
#include <stdio.h>
#include <cstring>    
#include <sys/socket.h>
#include <netdb.h>      
#include <unistd.h>

#define MAX_TRIES 3
#define DEFAULT_Q_CONNECTIONS 5
#define DEFAULT_PORT 49151
#define DEFAULT_PORT_STR "49151"
#define DEFAULT_BUFLEN 2048

int main()
{

    int status, socketfd, client_socket, port, tries;
    struct addrinfo host_info;       // The struct that getaddrinfo() fills up with data.
    struct addrinfo *host_info_list; // Pointer to the to the linked list of host_info's.
    struct sockaddr_storage client_addr;

	ssize_t bytes_received;
	char buffer[DEFAULT_BUFLEN];
	FILE *fstream; 
    // The MAN page of getaddrinfo() states "All  the other fields in the structure pointed
    // to by hints must contain either 0 or a null pointer, as appropriate." When a struct
    // is created in c++, it will be given a block of memory. This memory is not nessesary
    // empty. Therefor we use the memset function to make sure all fields are NULL.
    memset(&host_info, 0, sizeof host_info);

    host_info.ai_family = AF_UNSPEC;     // IP version not specified. Can be both.
    host_info.ai_socktype = SOCK_STREAM; // Use SOCK_STREAM for TCP or SOCK_DGRAM for UDP.
    host_info.ai_flags = AI_PASSIVE;     // IP Wildcard

	// NULL is for localhost, then we specify the port nomber
    status = getaddrinfo(NULL, DEFAULT_PORT_STR, &host_info, &host_info_list);
    // (translated into human readable text by the gai_gai_strerror function).
    if (status != 0){
	  	std::cerr << "Could not identify internet host: " << gai_strerror(status) ;
		return -1;
	}

    // Creating server socket
	tries = 0;
	do {
    	socketfd = socket(host_info_list->ai_family, host_info_list->ai_socktype,
                      host_info_list->ai_protocol);
		++tries;
	} while (socketfd == -1 && tries < MAX_TRIES);
    if (socketfd == -1){
		std::cerr << "Invalid socket descriptor." ;
		return -1;
	}  

    //std::cout << "Binding socket..."  << std::endl;
    // we use to make the setsockopt() function to make sure the port is not in use
    // by a previous execution of our code. (see man page for more information)
    int yes = 1;
    status = setsockopt(socketfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
	tries = 0;
	do{
    	status = bind(socketfd, host_info_list->ai_addr, host_info_list->ai_addrlen);
		++tries;
	} while (socketfd == -1 && tries < MAX_TRIES);
    if (status == -1){
		std::cerr << "Could not bind to socket." ;
		return -1;
	}  

    do {
    	//std::cerr << "\nWaiting for connections..."  << std::endl;
		status =  listen(socketfd, DEFAULT_Q_CONNECTIONS);
    	if (status == -1){
			std::cerr << "Could not listen to client." << std::endl;
			continue;
		}

    	socklen_t addr_size = sizeof(client_addr);
		tries = 0;
		do {
    		client_socket = accept(socketfd, (struct sockaddr *)&client_addr, &addr_size);
			++tries;	
		} while (client_socket == -1 && tries < MAX_TRIES);
		if (client_socket == -1 ){
			std::cerr << "Could not accept connection to client." << std::endl;
			continue;
		}
		//std::cerr << "Receiving data..." << std::endl;	
		fstream = fopen("/dev/fd/1","w");
		while ((bytes_received = recv(client_socket, buffer,DEFAULT_BUFLEN, 0)) > 0){
			//buffer[bytes_received] = '\0';
			fwrite (buffer, sizeof(char), bytes_received, fstream);	
		}

		//const char message[] = "Closing connection...";
		//size_t msg_len = sizeof(message);
		//count = write(client_socket, message, sizeof(message));
		close(client_socket);
		fclose(fstream);
	} while(1);	

	std::cerr << "Stopping server..." << std::endl;
	freeaddrinfo(host_info_list);
    close(socketfd);

	return 0;

}

