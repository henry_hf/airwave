#ifndef __AIRWAVE_WAVE
#define __AIRWAVE_WAVE

typedef unsigned int uint;
typedef unsigned char uchar;

class WavHeader{
	// Wav format has a 44byte header
	char _header[44];
public:
	void create(int channels, int bits_per_sample, int sample_per_sec);
	void set_field(uchar* buffer, uint begin, uint nbytes);
	void intbytes(uint* integer, uint nbytes); 
	int test();
	inline const char* get() const{ return _header; }
};

#endif