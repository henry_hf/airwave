#ifndef __AIRWAVE_CLIENT
#define __AIRWAVE_CLIENT

#include<iostream>
#include<stdio.h>
#include<winsock2.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

#define DEFAULT_BUFLEN 2048
#define DEFAULT_PORT 49151
#define DEFAULT_SERVER_ADDR "192.168.1.111"

class Client {
	WSADATA _wsaData;
	SOCKET _socket;
	struct sockaddr_in _server;
	const char *_server_addr;
	int _port;
public:
	Client();
	Client(char* server_addr, int port);
	int initiate();
	int write_socket(const char* bytes, unsigned long tobewritten);
	int read_socket(char* buffer, UINT length);
	int finish_sending();
	void close();
};

#endif