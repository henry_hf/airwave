/*
 *	Careful with windows.h, ws2def.h, and winsock.h:
 *	http://www.zachburlingame.com/2011/05/resolving-redefinition-errors-betwen-ws2def-h-and-winsock-h/
 *  Original sources http://www.blitter.com/~russtopia/MIDI/~jglatt/tech/lowaud.htm
 *	
 */
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <conio.h>
#include <mmsystem.h>
#include "Client.h"
#include "WavHeader.h"

#pragma comment(lib, "winmm.lib") // Wave librarys

/* Handle to the WAVE In Device */
HWAVEIN				WaveInHandle;

/* We use two WAVEHDR's for recording (ie, double-buffering) in this example */
WAVEHDR				WaveHeader[2];

/* Variable used to indicate whether we are in record */
BOOL				InRecord = FALSE;

/* Variable used by recording thread to indicate whether we are in record */
unsigned char		DoneAll;

/* Client socket */
Client client;

/* This function will be called in a background thread to record all audio
 * until user interruption. waveInProc() simply loops around a call to
 * GetMessage() to process those messages that the audio driver sends during recording.
 */

DWORD WINAPI waveInProc(LPVOID arg) {
	MSG		msg;

	/* Wait for a message sent to me by the audio driver */
	while (GetMessage(&msg, 0, 0, 0) == 1) {
		/* Figure out which message was sent */
		switch (msg.message) {
			/* A buffer has been filled by the driver */
			case MM_WIM_DATA: {
				/* msg.lParam contains a pointer to the WAVEHDR structure for the filled buffer.
				 * NOTE: dwBytesRecorded field specifies how many bytes of
				 * audio data are in the buffer even though it may not be full
				 */
				//if (!WriteFile(WaveFileHandle, ((WAVEHDR *)msg.lParam)->lpData, ((WAVEHDR *)msg.lParam)->dwBytesRecorded, &msg.time, 0) 
				if (((WAVEHDR *)msg.lParam)->dwBytesRecorded) {
					if (!client.write_socket(((WAVEHDR *)msg.lParam)->lpData, ((WAVEHDR *)msg.lParam)->dwBytesRecorded) ||
						msg.time != ((WAVEHDR *)msg.lParam)->dwBytesRecorded) { /* Check for an error */ }						
				}
				/* Are we still recording? */
				if (InRecord) {
					/* Requeue this buffer so the driver can use it for another block of audio.
					 */
					waveInAddBuffer(WaveInHandle, (WAVEHDR *)msg.lParam, sizeof(WAVEHDR));
				} else {					
					/* We aren't recording, so another WAVEHDR has been returned to us after recording has stopped.
					 * When we get all of them back, DoneAll will be equal to how many WAVEHDRs we queued
					 */
					++DoneAll;
				}

				/* Keep waiting for more messages */
                continue;
			}

			/* Our main thread is opening the WAVE device */
			case MM_WIM_OPEN: {
				/* Re-initialize 'DoneAll' and keep waiting for more messages */
				DoneAll = 0;
                continue;
			}

			/* Our main thread is closing the WAVE device */
			case MM_WIM_CLOSE: {
				/* Terminate this thread (by return'ing) */
				break;
			}
		}
	}

	return(0);
}

/* Display information about all recording devices */
void print_inputdevices(){
	WAVEINCAPS     wic;
	unsigned long  iNumDevs, i;

	/* Get the number of Digital Audio In devices in this computer */
	iNumDevs = waveInGetNumDevs();

	/* Go through all of those devices, displaying their names */
	for (i = 0; i < iNumDevs; i++)
	{
		/* Get info about the next device */
		if (!waveInGetDevCaps(i, &wic, sizeof(WAVEINCAPS)))
		{
			/* Display its Device ID and name */
			printf("Device ID #%u: %s\r\n", i, wic.szPname);
		 }
	}
}

void print_deviceinfo(HWAVEIN *handle){
	WAVEINCAPS wic;
	UINT id = 0;
	LPUINT lid = &id;
	// Store the device id in lid
	waveInGetID(*handle, lid);
	if (!waveInGetDevCaps(id, &wic, sizeof(WAVEOUTCAPS)))
	{
			/* Display its Device ID and name */
			printf("Device ID #%u: %s\r\n", id, wic.szPname);
	}
}

int main(int argc , char ** argv)
{
	MMRESULT		err;
	WAVEFORMATEX	waveFormat;
	HANDLE			waveInThread;
	WavHeader		wavHeader;
	UINT			recDevID;

	/* Create our thread that we use to store our incoming "blocks" of digital audio data (sent to us
	 * from the driver. The main procedure of this thread is waveInProc(). We need to get the threadID
	 * and pass that to waveInOpen(). CreateThread() will give it to us.
	 */
	waveInThread = CreateThread(0, 0, (LPTHREAD_START_ROUTINE)waveInProc, 0, 0, (LPDWORD)&err);
	if (!waveInThread) {
		printf("Can't create WAVE recording thread! -- %08X\n", GetLastError());
		return(-1);
	}
	CloseHandle(waveInThread);
	
	/* Clear out both of our WAVEHDRs. At the very least, waveInPrepareHeader() expects the dwFlags field to
	 * be cleared
	 */
	ZeroMemory(&WaveHeader[0], sizeof(WAVEHDR) * 2);

	/* Initialize the WAVEFORMATEX for 16-bit, 44KHz, stereo. That's what I want to record */
	waveFormat.wFormatTag = WAVE_FORMAT_PCM;
	waveFormat.nChannels = 2;
	waveFormat.nSamplesPerSec = 192000;
	waveFormat.wBitsPerSample = 24;
	waveFormat.nBlockAlign = waveFormat.nChannels * (waveFormat.wBitsPerSample/8);
	waveFormat.nAvgBytesPerSec = waveFormat.nSamplesPerSec * waveFormat.nBlockAlign;
	waveFormat.cbSize = 0;

	//print_inputdevices(); // Shows Stereo Mix is device #2
	recDevID = 2;
	err = waveInOpen(&WaveInHandle, recDevID, &waveFormat, (DWORD)err, 0, CALLBACK_THREAD);
	if (err) { 
		printf("Can't open WAVE In Device!");
		return(-2);
	} else {
		//print_deviceinfo(&WaveInHandle);
		printf("Press ENTER to start recording.\n\n");
		getchar();
		/* Now that the inputs are all set up, we can record an audio file using waveInPrepareHeader(),
		 * waveInAddBuffer(), and waveInStart().
		 *
		 * Establish connection to the server. 
		 */
		if (!client.initiate()) {
			wavHeader.create(waveFormat.nChannels, waveFormat.wBitsPerSample, waveFormat.nSamplesPerSec);
			client.write_socket(wavHeader.get(), 44);

			/* Allocate, prepare, and queue two buffers that the driver can use to record blocks of audio data. */
			WaveHeader[1].dwBufferLength = WaveHeader[0].dwBufferLength = waveFormat.nAvgBytesPerSec << 1;
			if (!(WaveHeader[0].lpData = (char *)VirtualAlloc(0, WaveHeader[0].dwBufferLength * 2, MEM_COMMIT, PAGE_READWRITE))) {
				printf("ERROR: Can't allocate memory for WAVE buffer!\n");
			} else {
				/* Fill in WAVEHDR fields for buffer starting address. We've already filled in the size fields above. */
				WaveHeader[1].lpData = WaveHeader[0].lpData + WaveHeader[0].dwBufferLength;

				/* Leave other WAVEHDR fields at 0 */
				/* Prepare the 2 WAVEHDR's */
				if ((err = waveInPrepareHeader(WaveInHandle, &WaveHeader[0], sizeof(WAVEHDR)))) {
					printf("Error preparing WAVEHDR 1! -- %08X\n", err);
				} else {
					if ((err = waveInPrepareHeader(WaveInHandle, &WaveHeader[1], sizeof(WAVEHDR)))) {
						printf("Error preparing WAVEHDR 2! -- %08X\n", err);
					} else { /* Queue first WAVEHDR (recording hasn't started yet) */
						if ((err = waveInAddBuffer(WaveInHandle, &WaveHeader[0], sizeof(WAVEHDR)))) {
							printf("Error queueing WAVEHDR 1! -- %08X\n", err);
						} else { /* Queue second WAVEHDR */
							if ((err = waveInAddBuffer(WaveInHandle, &WaveHeader[1], sizeof(WAVEHDR)))) {
								printf("Error queueing WAVEHDR 2! -- %08X\n", err);
								DoneAll = 1;
								goto abort;
							} else { /* Start recording. Our secondary thread will now be receiving
									  * and storing audio data to disk */
								InRecord = TRUE;
								if ((err = waveInStart(WaveInHandle))) {
									printf("Error starting record! -- %08X\n", err);
								} else { /* Wait for user to stop recording */
									printf("Recording has started. Press ENTER key to stop recording...\n");
									getchar();
								}

								/* Stop recording and tell the driver to unqueue/return all of our WAVEHDRs. */
abort:							InRecord = FALSE;
								waveInReset(WaveInHandle);

								/* Wait for the recording thread to receive the MM_WIM_DONE for each
								 * of our queued WAVEHDRs. It will count them off, and after processing
								 * them all, 'DoneAll' will be equal to how many WAVEHDRs we used. We
								 * can then unprepare the WAVEHDRs, close our WAVE device and our disk
								 * file, and free our buffers
								 */
								while (DoneAll < 2) Sleep(100);
							}
						}

						/* Unprepare second WAVEHDR. */
						if ((err = waveInPrepareHeader(WaveInHandle, &WaveHeader[1], sizeof(WAVEHDR)))) {
							printf("Error unpreparing WAVEHDR 2! -- %08X\n", err);
						}
					}

					/* Unprepare first WAVEHDR */
					if ((err = waveInPrepareHeader(WaveInHandle, &WaveHeader[0], sizeof(WAVEHDR)))) {
						printf("Error unpreparing WAVEHDR 1! -- %08X\n", err);
					}
				}
			}
		}
	}

	/* Close the WAVE In device */
	while(waveInClose(WaveInHandle)){
		printf("Can't close Wave In Device");
	}

	/* Close the output stream */
	//if (WaveFileHandle != INVALID_HANDLE_VALUE) CloseHandle(WaveFileHandle);
	client.close();

	/* Free any memory allocated for our buffers */
	if (WaveHeader[0].lpData) VirtualFree(WaveHeader[0].lpData, 0, MEM_RELEASE);

	return(0);
}
