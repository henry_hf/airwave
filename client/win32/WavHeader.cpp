#include <cstdio>
#include "WavHeader.h"

// Set header with values from buffer, starting from 'begin' to 'begin+nbytes'
void WavHeader::set_field(uchar* buffer, uint begin, uint nbytes){
	for(uint i = 0; i < nbytes; ++i)
		_header[begin+i] = buffer[i];
}

void WavHeader::create(int channels, int bits_per_sample, int sample_per_sec){
	
	uint integer = 0;
	uchar *ibytes = reinterpret_cast<uchar*>(&integer);
	//   0-3: "RIFF" (0x52494646 big endian)
	set_field((uchar*)"RIFF", 0, 4);
	//   4-7: size of the file in bytes minus 8 bytes (little endian)
	//// 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size)
	//// Usually this field is set when the recording is over, but our purpose is streaming so 0 will do.
	integer = 0;
	set_field(ibytes, 4, 4);
	//  8-11: "WAVE":0x57415645 (big endian)
	set_field((uchar*)"WAVE", 8, 4);
	// 12-14: "fmt ":0x666d7420 (big endian)
	set_field((uchar*)"fmt ", 12, 4);
	// 16-19: 16:0x10 for PCM. The size of the rest of the Subchunk which follows this number.
	integer = 16;
	set_field(ibytes, 16, 4);
	// 20-21: 1:0x01 (type of format, 1:PCM)
	integer = 1;
	set_field(ibytes, 20, 2);
	// 22-23: 2:0x02 (channels)
	integer = channels;
	set_field(ibytes, 22, 2);
	// 24-27: 44100:0x44ac;  192000:0x00ee0200 (sample rate, little endian)
	integer = (uint)sample_per_sec;
	set_field(ibytes, 24, 4);
	// 28-31: 16bit 44100Hz -> 176400:0x10b102; 24bit 192000Hz -> 1152000:0x009411
	//        (sample_rate*bits_per_sample*channels/8, little endian)
	integer = (uint)(sample_per_sec*bits_per_sample*channels/8);
	set_field(ibytes, 28, 4);
	// 32-33: Bytes per sample, including all channels. 16 bit -> 4:0x04; 24bit -> 6:0x06
	//        (bits_per_smpl*channels/8, little endian).
	integer = (uint)bits_per_sample*channels/8;
	set_field(ibytes, 32, 2);
	// 34-35: 16:0x10; 24:0x18 (bits per sample, little endian) 
	integer = bits_per_sample;
	set_field(ibytes, 34, 2);
	// 36-39: "data":0x64617461 (big endian)
	set_field((uchar*)"data", 36, 4);
	// 40-43: data size (little endian)
	integer = 0;
	set_field(ibytes, 40, 4);
}

/* Treat integers as unsigned char arrays. 
	Print in bigendian mode the first 'nbytes' bytes, if 'nbytes' is greater than 0.
	This is, indeed, dark magic waiting to explode */
void WavHeader::intbytes(uint* integer, uint nbytes){
	uchar *ibytes = reinterpret_cast<uchar*>(integer);

	if (nbytes > 0){
		printf("%d: ", *integer);
		for(uint i = 0; i < nbytes; ++i){
			printf("0x%02x, ", ibytes[i]);
		}
		putchar('\n');
	}
}

int WavHeader::test(){
	uint myint[] = {44100, 44800, 88200, 96000, 192000, 16, 24, 2, 4};
	int nint = sizeof(myint) / sizeof(myint[0]);
	for (int k = 0; k < nint; ++k){
		intbytes(&myint[k], 4);		
	}
	create(2, 24, 192000);
	for (uint i = 0; i < 44; ++i){
		printf("x%02x ",_header[i]);
		if ((i % 4) == 3) putchar('\t');
		if ((i % 8) == 7) putchar('\n');
	}
	getchar();
	return 0;
}