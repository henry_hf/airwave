#include "Client.h"

Client::Client() {
	_socket = INVALID_SOCKET;
	_server_addr = DEFAULT_SERVER_ADDR;
	_port = DEFAULT_PORT;
}

Client::Client(char* server_addr, int port){
	_socket = INVALID_SOCKET;
	_server_addr = server_addr;
	_port = port;
}

int Client::initiate() {

	int result;
	printf("Establishing connection to server...\n");

    // Initialize Winsock	
	result = WSAStartup(MAKEWORD(2,2),&_wsaData);
	if ( result != NO_ERROR) {
		printf("WSAStartup failed: %d\n", result);
		return 1;
	}

	
	//Create a socket
	_socket = socket(AF_INET , SOCK_STREAM , IPPROTO_TCP);
	//_socket = socket(AF_INET , SOCK_DGRAM , IPPROTO_UDP);
	if(_socket == INVALID_SOCKET) {
        printf("Error at socket(): %ld\n", WSAGetLastError() );
        WSACleanup();
        return 1;
	}

    // The sockaddr_in structure specifies the address family,
    // IP address, and port of the server to be connected to.	
	_server.sin_family = AF_INET;
	_server.sin_addr.s_addr = inet_addr( _server_addr );
	_server.sin_port = htons( _port );

	//Connect to remote server
	result = connect(_socket , (struct sockaddr *)&_server , sizeof(_server));
	if (result == SOCKET_ERROR) {
        printf("Unable to connect to server: %ld\n", WSAGetLastError());
		close();
        return 1;
	}
	// puts("Connected.");
	return 0;
}

int Client::write_socket(const char* buffer, unsigned long tobewritten){
	int result = send(_socket , buffer, tobewritten, 0);
	if(result == SOCKET_ERROR) {
        printf("Send failed: %d\n", WSAGetLastError());
		close();
        return 1;
	}
	// printf("Bytes Sent: %ld\n", result);
	return result;
}

int Client::read_socket(char* buffer, UINT length){
	//Receive a reply from the server
	int result = 0;
	do {
		result = recv(_socket, buffer, length, 0);
        if ( result > 0 ){
            printf("Bytes received: %d\n", result);
			buffer[result] = '\0';
			puts(buffer);
		}
        else if ( result == 0 )
            printf("Connection closed\n");
        else
            printf("recv failed: %d\n", WSAGetLastError());
	} while (result > 0);
	close();
	return 0;
}

int Client::finish_sending(){
	int result;
    result = shutdown(_socket, SD_SEND);
    if (result == SOCKET_ERROR) {
        printf("shutdown failed: %d\n", WSAGetLastError());
        close();
        return 1;
    }
	return 0;
}

void Client::close(){
	closesocket(_socket);
    WSACleanup();
}